﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Biblioteczka_domowa_Homelib
{
    /// <summary>
    /// Logika interakcji dla klasy library.xaml
    /// </summary>
    public partial class library : Window
    {
        public library()
        {
            InitializeComponent();
            BiblioteczkaEntities db = new BiblioteczkaEntities();
            var books = from b in db.Book
                        select b;
            foreach (var item in books)
            {
                Console.Write(item.Lp);
                Console.Write(item.Autor);
                Console.Write(item.Tytuł);
                Console.Write(item.Gatunek);
                Console.Write(item.W_domu);
                Console.Write(item.Pożyczona);
                Console.Write(item.Wypożyczona_od);
            }
            this.BookGrid.ItemsSource = books.ToList();
        }
        private void CloseBTN_Click(object sender, RoutedEventArgs e)
        {
            Close();
            Application.Current.Shutdown();
        }
        private void MaximizeBTN_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Maximized;
            }
            else
            {
                if (WindowState == WindowState.Maximized)
                {
                    WindowState = WindowState.Normal;
                }
            }
        }
        private void MinimizeBTN_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Minimized;
            }
        }
        private void HomeBTN_Click(object sender, RoutedEventArgs e)
        {
            MainWindow home = new MainWindow();
            Visibility = Visibility.Hidden;
            home.Show();
        }

        private void LibraryBTN_Click(object sender, RoutedEventArgs e)
        {
            library lib = new library();
            Visibility = Visibility.Hidden;
            lib.Show();
        }

        private void AddBTN_Click(object sender, RoutedEventArgs e)
        {
            Addbook add = new Addbook();
            Visibility = Visibility.Hidden;
            add.Show();
        }
        private void ColorChangeBTN_Click(object sender, RoutedEventArgs e)
        {
            if (Tlo.Background == Brushes.Black)
            {
                Tlo.Background = Brushes.DarkSlateBlue;
            }
            else
            {
                Tlo.Background = Brushes.Black;
            }
        }

        private int updatingbookID = 0;
        private void BookGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.BookGrid.SelectedIndex >= 0)
            {
                if (this.BookGrid.SelectedItems.Count >= 0)
            {
                if (this.BookGrid.SelectedItems[0].GetType() == typeof(Book))
                    {
                     Book b = (Book)this.BookGrid.SelectedItems[0];
                     this.AutorTXT.Text = b.Autor;
                     this.TitleTXT.Text = b.Tytuł;
                     this.GenreTXT.Text = b.Gatunek;
                     this.InhomeBool.IsChecked = b.W_domu;
                     this.LendTXT.Text = b.Pożyczona;
                     this.BorrowTXT.Text = b.Wypożyczona_od;
                     this.updatingbookID = b.Lp;
                    }
            }
            }
        }
        private void UpdateBTN_Click(object sender, RoutedEventArgs e)
        {
            BiblioteczkaEntities db = new BiblioteczkaEntities();
            var r = from b in db.Book
                    where b.Lp == this.updatingbookID
                    select b;
            Book obj = r.SingleOrDefault();
            if (obj != null)
            {
                obj.Autor = this.AutorTXT.Text;
                obj.Tytuł = this.TitleTXT.Text;
                obj.Gatunek = this.GenreTXT.Text;
                obj.W_domu = this.InhomeBool.IsChecked;
                obj.Pożyczona = this.LendTXT.Text;
                obj.Wypożyczona_od = this.BorrowTXT.Text;
                db.SaveChanges();
            }
        }

        private void LoadBTN_Click(object sender, RoutedEventArgs e)
        {
            BiblioteczkaEntities db = new BiblioteczkaEntities();
            this.BookGrid.ItemsSource = db.Book.ToList();

        }

        private void DeleteBTN_Click(object sender, RoutedEventArgs e)
        {
             MessageBoxResult msgboxresult = MessageBox.Show("Czy na pewno chcesz usunąć tą książkę?","Usuń książkę",
                 MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No);
            if (msgboxresult == MessageBoxResult.Yes)
            {
                BiblioteczkaEntities db = new BiblioteczkaEntities();
                var r = from b in db.Book
                        where b.Lp == this.updatingbookID
                        select b;
                Book obj = r.SingleOrDefault();
                if (obj != null)
                {
                    db.Book.Remove(obj);
                    db.SaveChanges();
                }
            }
        }
    }
}

