﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Biblioteczka_domowa_Homelib
{
    /// <summary>
    /// Logika interakcji dla klasy Addbook.xaml
    /// </summary>
    public partial class Addbook : Window
    {
        public Addbook()
        {
            InitializeComponent();

        }
        private void CloseBTN_Click(object sender, RoutedEventArgs e)
        {
            Close();
            Application.Current.Shutdown();
        }
        private void MaximizeBTN_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Maximized;
            }
            else
            {
                if (WindowState == WindowState.Maximized)
                {
                    WindowState = WindowState.Normal;
                }
            }
        }
        private void MinimizeBTN_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Minimized;
            }
        }

        private void HomeBTN_Click(object sender, RoutedEventArgs e)
        {
            MainWindow home = new MainWindow();
            Visibility = Visibility.Hidden;
            home.Show();
        }

        private void LibraryBTN_Click(object sender, RoutedEventArgs e)
        {
            library lib = new library();
            Visibility = Visibility.Hidden;
            lib.Show();
        }

        private void AddBTN_Click(object sender, RoutedEventArgs e)
        {

        }
        private void ColorChangeBTN_Click(object sender, RoutedEventArgs e)
        {
            if(Tlo.Background == Brushes.Black)
            {
                Tlo.Background = Brushes.DarkSlateBlue;
            }
            else
            {
                Tlo.Background = Brushes.Black;
            }
        }

        private void AcceptBTN_Click(object sender, RoutedEventArgs e)
        {
            BiblioteczkaEntities db = new BiblioteczkaEntities();
            Book biblioteczkaobject = new Book()
            {
                Autor = AutorTXT.Text,
                Tytuł = TitleTXT.Text,
                Gatunek = GenreTXT.Text,
                W_domu = InhomeBool.IsChecked,
                Pożyczona =LendTXT.Text,
                Wypożyczona_od=BorrowTXT.Text,
            };db.Book.Add(biblioteczkaobject);
            db.SaveChanges();
        }
    }
}
