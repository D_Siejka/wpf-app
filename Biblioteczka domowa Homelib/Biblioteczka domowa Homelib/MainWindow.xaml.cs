﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Biblioteczka_domowa_Homelib
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        /// Close_Button
        private void CloseBTN_Click(object sender, RoutedEventArgs e)
        {
            Close();
            Application.Current.Shutdown();
        }

        private void MaximizeBTN_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Maximized;
            }
            else
            {
                if (WindowState == WindowState.Maximized)
                {
                    WindowState = WindowState.Normal;
                }
            }
        }

        private void MinimizeBTN_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Normal)
            {
                WindowState = WindowState.Minimized;
            }
        }
        private void HomeBTN_Click(object sender, RoutedEventArgs e)
        {

        }

        private void LibraryBTN_Click(object sender, RoutedEventArgs e)
        {
            library lib = new library();
            Visibility = Visibility.Hidden;
            lib.Show();
        }

        private void AddBTN_Click(object sender, RoutedEventArgs e)
        {
            Addbook add = new Addbook();
            Visibility = Visibility.Hidden;
            add.Show();
        }

        private void ColorChangeBTN_Click(object sender, RoutedEventArgs e)
        {
            if(Tlo.Background == Brushes.Black)
            {
                Tlo.Background = Brushes.DarkSlateBlue;
            }
            else
            {
                Tlo.Background = Brushes.Black;
            }
        }
    }
}

